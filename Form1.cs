namespace WeatherApp {
    public partial class FormMain : Form {
        private Vrijeme vrijeme;

        public FormMain() {
            InitializeComponent();
            vrijeme = new Vrijeme();
            vrijeme.DataRefresh += RefreshPrikaz;

            RefreshGradovi(); // refreshaj popis
            RefreshWarmCold(); // refreshaj najtoplije i najhladnije
        }

        // osvjezi gradove za combobox
        private void RefreshGradovi() {
            cboxCity.Items.Clear();

            foreach(var grad in vrijeme.GetGradovi()) {
                cboxCity.Items.Add(grad);
            }
        }

        // list boxevi - will finish
        private void RefreshWarmCold() {
            lbTopli.Items.Clear();
            lbHladni.Items.Clear();

            lbTopli.Items.AddRange(vrijeme.GetWarmest(5).ToArray());
            lbHladni.Items.AddRange(vrijeme.GetColdest(5).ToArray());
        }

        // pokazi trazeni info u formi
        private void cboxCity_SelectedIndexChanged(object sender, EventArgs e) {
            var podatak = vrijeme.GetAll(cboxCity.Text);

            lblGrad.Text = cboxCity.SelectedItem.ToString();
            lblTemp.Text = podatak.TemperaturaHuman;
            lblHumid.Text = podatak.VlagaHuman;
            lblPress.Text = podatak.TlakHuman;     
        }

        // refreshaj prikaz (lab07):
        public void RefreshPrikaz(object sender, VrijemeArgs e) {
            if (this.InvokeRequired) {
                BeginInvoke(RefreshGradovi);
                BeginInvoke(RefreshWarmCold);

                lblSSupdate.Text = e.DateRefreshHuman;
                
                // [MINOR BUG]
                // prilikom starta app lblSSupdate nece pokazivati nista
                // tek nakon 30 sec (nakon prvog refresha) ce poceti pokazivati
                // kad je zadnji put updateovao.. minor bug, patchevat cu kasnije

                // [UPDATE 1 NA BUG]:
                // Izgleda da je build popravio gresku...?
                // Nonetheless, ostavim report tu za slucaj da ipak nije fixano
            }
        }

    }
}