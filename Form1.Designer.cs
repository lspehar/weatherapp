namespace WeatherApp {
    partial class FormMain {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.cboxCity = new System.Windows.Forms.ComboBox();
            this.lblGrad = new System.Windows.Forms.Label();
            this.lblTemp = new System.Windows.Forms.Label();
            this.lblHumid = new System.Windows.Forms.Label();
            this.lblPress = new System.Windows.Forms.Label();
            this.lblTopli = new System.Windows.Forms.Label();
            this.lblHladni = new System.Windows.Forms.Label();
            this.lbTopli = new System.Windows.Forms.ListBox();
            this.lbHladni = new System.Windows.Forms.ListBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblSSupdate = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cboxCity
            // 
            this.cboxCity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cboxCity.FormattingEnabled = true;
            this.cboxCity.Location = new System.Drawing.Point(165, 25);
            this.cboxCity.Name = "cboxCity";
            this.cboxCity.Size = new System.Drawing.Size(203, 23);
            this.cboxCity.TabIndex = 1;
            this.cboxCity.SelectedIndexChanged += new System.EventHandler(this.cboxCity_SelectedIndexChanged);
            // 
            // lblGrad
            // 
            this.lblGrad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGrad.BackColor = System.Drawing.Color.Transparent;
            this.lblGrad.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblGrad.Location = new System.Drawing.Point(12, 75);
            this.lblGrad.Name = "lblGrad";
            this.lblGrad.Size = new System.Drawing.Size(510, 45);
            this.lblGrad.TabIndex = 2;
            this.lblGrad.Text = "Odaberi Grad!";
            this.lblGrad.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTemp
            // 
            this.lblTemp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTemp.BackColor = System.Drawing.Color.Transparent;
            this.lblTemp.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTemp.Location = new System.Drawing.Point(12, 140);
            this.lblTemp.Name = "lblTemp";
            this.lblTemp.Size = new System.Drawing.Size(510, 32);
            this.lblTemp.TabIndex = 3;
            this.lblTemp.Text = "teperatura";
            this.lblTemp.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblHumid
            // 
            this.lblHumid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHumid.BackColor = System.Drawing.Color.Transparent;
            this.lblHumid.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblHumid.Location = new System.Drawing.Point(12, 189);
            this.lblHumid.Name = "lblHumid";
            this.lblHumid.Size = new System.Drawing.Size(510, 30);
            this.lblHumid.TabIndex = 4;
            this.lblHumid.Text = "vlaznost";
            this.lblHumid.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblPress
            // 
            this.lblPress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPress.BackColor = System.Drawing.Color.Transparent;
            this.lblPress.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblPress.Location = new System.Drawing.Point(12, 235);
            this.lblPress.Name = "lblPress";
            this.lblPress.Size = new System.Drawing.Size(510, 30);
            this.lblPress.TabIndex = 5;
            this.lblPress.Text = "pritisak";
            this.lblPress.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTopli
            // 
            this.lblTopli.AutoSize = true;
            this.lblTopli.Location = new System.Drawing.Point(12, 306);
            this.lblTopli.Name = "lblTopli";
            this.lblTopli.Size = new System.Drawing.Size(101, 15);
            this.lblTopli.TabIndex = 6;
            this.lblTopli.Text = "Najtopliji gradovi:";
            // 
            // lblHladni
            // 
            this.lblHladni.AutoSize = true;
            this.lblHladni.Location = new System.Drawing.Point(285, 306);
            this.lblHladni.Name = "lblHladni";
            this.lblHladni.Size = new System.Drawing.Size(110, 15);
            this.lblHladni.TabIndex = 7;
            this.lblHladni.Text = "Najhladniji gradovi:";
            // 
            // lbTopli
            // 
            this.lbTopli.FormattingEnabled = true;
            this.lbTopli.ItemHeight = 15;
            this.lbTopli.Location = new System.Drawing.Point(12, 324);
            this.lbTopli.Name = "lbTopli";
            this.lbTopli.Size = new System.Drawing.Size(237, 109);
            this.lbTopli.TabIndex = 8;
            // 
            // lbHladni
            // 
            this.lbHladni.FormattingEnabled = true;
            this.lbHladni.ItemHeight = 15;
            this.lbHladni.Location = new System.Drawing.Point(285, 324);
            this.lbHladni.Name = "lbHladni";
            this.lbHladni.Size = new System.Drawing.Size(237, 109);
            this.lbHladni.TabIndex = 9;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblSSupdate});
            this.statusStrip1.Location = new System.Drawing.Point(0, 439);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(534, 22);
            this.statusStrip1.TabIndex = 10;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblSSupdate
            // 
            this.lblSSupdate.Name = "lblSSupdate";
            this.lblSSupdate.Size = new System.Drawing.Size(83, 17);
            this.lblSSupdate.Text = "Zadnji update:";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 461);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.lbHladni);
            this.Controls.Add(this.lbTopli);
            this.Controls.Add(this.lblHladni);
            this.Controls.Add(this.lblTopli);
            this.Controls.Add(this.lblPress);
            this.Controls.Add(this.lblHumid);
            this.Controls.Add(this.lblTemp);
            this.Controls.Add(this.lblGrad);
            this.Controls.Add(this.cboxCity);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WeatherApp";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComboBox cboxCity;
        private Label lblGrad;
        private Label lblTemp;
        private Label lblHumid;
        private Label lblPress;
        private Label lblTopli;
        private Label lblHladni;
        private ListBox lbTopli;
        private ListBox lbHladni;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel lblSSupdate;
    }
}
