using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WeatherApp {
    class Vrijeme {
        public Vrijeme() {
            data = DohvatiPodatke();

            // thread za update:
            Thread refreshData = new Thread(Refresh);
            refreshData.IsBackground = true; // refreshaj u backgroundu
            refreshData.Start(); // startaj!
        }

        public List<VrijemePodatak> DohvatiPodatke() {
            XmlDocument vrijeme = new XmlDocument();
            vrijeme.Load("http://vrijeme.hr/hrvatska_n.xml");

            List<VrijemePodatak> podaci = new List<VrijemePodatak>();

            XmlNodeList gradovi = vrijeme.GetElementsByTagName("Grad");
            foreach (XmlNode grad in gradovi) {
                podaci.Add(new VrijemePodatak(
                    grad["GradIme"].InnerText,
                    grad["Podatci"]["Temp"].InnerText,
                    grad["Podatci"]["Vlaga"].InnerText,
                    grad["Podatci"]["Tlak"].InnerText
                ));
            }
            return podaci;
        }

        public IEnumerable<string> GetGradovi() {
            var podaci = DohvatiPodatke();
            var gradovi =
                from grad in podaci
                orderby grad.Grad
                select grad.Grad;

            return gradovi;
        }

        public VrijemePodatak GetAll(string unos) {
            var podaci = DohvatiPodatke();
            var gradovi =
                (from grad in podaci
                 orderby grad.Temperatura
                 where grad.Grad == unos
                 select grad).First();

            return gradovi;
        }

        // update podataka:
        public void Refresh() {
            while (true) {
                data = DohvatiPodatke();
                DataRefresh?.Invoke(this, new VrijemeArgs());
                Thread.Sleep(30000);
            }
        }

        // boxevi za najtoplije i najhladnije:
        public IEnumerable<VrijemePodatak> GetWarmest(int num) {
            return (from d in data
                    orderby d.Temperatura descending
                    select d).Take(num);
        }

        public IEnumerable<VrijemePodatak> GetColdest(int num) {
            return (from d in data
                    orderby d.Temperatura
                    select d).Take(num);
        }

        private List<VrijemePodatak>? data;

        // delegat (lab07):
        public delegate void DataRefreshDelegate(object sender, VrijemeArgs e);
        public event DataRefreshDelegate? DataRefresh;
    }
}