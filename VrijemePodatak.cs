using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApp {
    class VrijemePodatak {
        public VrijemePodatak(string grad, string temperatura, string vlaga, string tlak) {
            Grad = grad;
            Temperatura = temperatura;
            Vlaga = vlaga;
            Tlak = tlak;
        }

        public string TemperaturaHuman {
            get { return ("Temperatura: " + Temperatura + " C").Trim(); }
        }

        public string VlagaHuman {
            get { return ("Vlaga: " + Vlaga + " %").Trim(); }
        }

        public string TlakHuman {
            get { return ("Tlak: " + Tlak + " hPa").Trim(); }
        }

        public override string ToString() {
            return $"{Grad}, {TemperaturaHuman}"; // moze to i ljepse ali ovako je ok
        }

        public string Grad { get; set; }
        public string Temperatura { get; set; }
        public string Vlaga { get; set; }
        public string Tlak { get; set; }
    }
}