using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApp {
    public class VrijemeArgs : EventArgs {
        // NOTE: znam da je ovo za lab07, al hocu imat kompletan program u kojem sve radi

        public VrijemeArgs() {
            DateRefresh = DateTime.Now;
        }

        public DateTime DateRefresh { get; }
        public string DateRefreshHuman { get => "Zadnje updateovano: " + DateRefresh.ToString(); }

    }
}
